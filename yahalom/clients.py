import asyncio
import functools
import websockets
import random
from Crypto.Cipher import AES
import rsa # используем симметричную криптографию вместо этого
import json
import os
import base64
import time

# AES криптография
def pad(byte_array):
    BLOCK_SIZE = 16
    pad_len = BLOCK_SIZE - len(byte_array) % BLOCK_SIZE
    return byte_array + (bytes([pad_len]) * pad_len)
def unpad(byte_array):
    last_byte = byte_array[-1]
    return byte_array[0:-last_byte]
def encrypt(key, iv, message):
    byte_array = str(message).encode("UTF-8")
    padded = pad(byte_array)
    obj = AES.new(key.encode("UTF-8"), AES.MODE_CBC,iv.encode('UTF-8'))
    ciphertext = obj.encrypt(padded) #шифртекст как биты
    ct1 = base64.b64encode(ciphertext).decode("UTF-8") #шифртекст как строка
    return(ct1)
def decrypt(key, iv, message):
    byte_array = base64.b64decode(message)
    cipher = AES.new(key.encode("UTF-8"), AES.MODE_CBC,iv.encode('UTF-8'))
    decrypted_padded = cipher.decrypt(byte_array)
    decrypted = unpad(decrypted_padded)
    text = decrypted.decode("UTF-8")
    return(text)

#шаг 2 (Боб отправляет Тренту (серверу) свой ID и сообщение Алисы со своим рандомным числом, 
#       которые зашифрованы симметричным ключом Боба (его знают Боб и Трент))
async def bobu(websocket, path, stop_request, trentport):
    keys = open("keys.txt", 'r')
    line=keys.readlines()
    Bob_key=line[2]
    Bob_iv=line[3]
    keys.close
    arisu_message = json.loads(await websocket.recv())
    uri = "ws://localhost:" + trentport
    trent = await websockets.connect(uri)
    nonce = random.randrange(0,8192) #ID Боба
    print("Generated nonce: " + str(nonce))
    arisu_message["nonceb"] = nonce
    encrypted_data = encrypt(Bob_key, Bob_iv, arisu_message)
    # data нужно шифровать
    bobu_message = {
        "reciever": "bobu", # ID Боба
        "data": encrypted_data # ID Алисы, её рандомное число, рандомное число Боба зашифрованы ключом Боба
    }
    print("Step 2 message: " + json.dumps(bobu_message))
    await trent.send(json.dumps(bobu_message))
    stop_request.set()

#шаг 5 (Боб проверяет, что его рандомное число не поменялось(?))
async def bobu_2(websocket, path, stop_request):
    keys = open("keys.txt", 'r')
    line=keys.readlines()
    Bob_key=line[2]
    Bob_iv=line[3]
    keys.close
    while True:
        try:
            finalfinalmessage = await websocket.recv()
            break
        except:
            time.sleep(2)
    finalfinalmessage = json.loads(finalfinalmessage)
    keypart = finalfinalmessage['keypart']
    keypart = decrypt(Bob_key, Bob_iv, keypart)
    keypart = json.loads(keypart.replace('\'', '\"'))
    session_key = keypart['session_key']
    print("Session key: " + session_key)
    nonce = decrypt(session_key, session_key, finalfinalmessage['nonce']) #он зашифрован новым ключом
    print("Recieved nonce: " + str(nonce))
    stop_request.set()

#шаг 4 (Алиса посылает Бобу свои ID и ключ сессии, зашифрованные ключом Алисы,
#       рандомное число Боба, зашифрованное ключом сессии)
#       также подтверждает, что её рандомное число не изменилось
async def arisu_handler(websocket, path, stop_request, bobport, nonce):
    keys = open("keys.txt", 'r')
    line=keys.readlines()
    Alice_key=line[0]
    Alice_iv=line[1]
    keys.close()
    print("waiting for final")
    trentmessage = await websocket.recv()
    decode_message = json.loads(trentmessage)
    forbob = decode_message[1]
    forme = decrypt(Alice_key, Alice_iv, decode_message[0])
    forme = json.loads(forme.replace('\'', '\"'))
    print(forme)
    nonceb = forme["nonceb"]
    noncea = forme["noncea"]
    session_key = forme["session_key"]
    if nonce == noncea:
        print ("My nonce: " + str(nonce) + " recieved nonce: " + str(noncea))
    else:
        print ("Check failed")
    finalfinalmessage = {
        "keypart": forbob,
        "nonce": encrypt(session_key, session_key, nonceb)
    }
    uri = "ws://localhost:" + bobport
    print("Step 4 message: " + json.dumps(finalfinalmessage))
    bob = await websockets.connect(uri)
    await bob.send(json.dumps(finalfinalmessage))
    stop_request.set()
    
#шаг 1 (Алиса отправляет свой ID и рандомное число Бобу)
async def arisu(arisuport, trentport):
    bobport = '8888'
    uri = "ws://localhost:" + bobport
    bob = await websockets.connect(uri)
    nonce = random.randrange(0,8192) #ID Алисы
    message = {
        "sender": 'arisu',
        "noncea": nonce
    }
    message = json.dumps(message)
    print("Step 1 message: " + message)
    await bob.send(message)
    stop_request = asyncio.Event()
    arisu_custom_handler = functools.partial(arisu_handler, stop_request = stop_request, bobport = bobport, nonce = nonce)
    arisuserver = await websockets.serve(arisu_custom_handler, "localhost", arisuport)
    await stop_request.wait()
    stop_request.clear()
    arisuserver.close()

async def main():
    exit_switch = 0
    trentport = input("Input Trent's port: ")
    while exit_switch == 0:
        switch = input('Enter mode (rec, send or exit): ')
        if switch == 'rec':
            bobuport = 8888
            stop_request = asyncio.Event()
            bobu_handler = functools.partial(bobu, stop_request = stop_request, trentport = trentport)
            server = await websockets.serve(bobu_handler, "localhost", bobuport)
            await stop_request.wait()
            stop_request.clear()
            server.close()
            stop_request = asyncio.Event()
            bobu_handler = functools.partial(bobu_2, stop_request = stop_request)
            server = await websockets.serve(bobu_handler, "localhost", bobuport)
            await stop_request.wait()
            stop_request.clear()
            server.close()
        elif switch == 'send':
            arisuport = 9999
            await arisu(arisuport, trentport)
        elif switch == 'exit':
            exit_switch = 1

asyncio.run(main())
