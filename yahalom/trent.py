import asyncio
import functools
import websockets
import random
import rsa # используем симметричную криптографию вместо этого
import json
import time
import os
from Crypto.Cipher import AES
import base64
import random, string

# AES криптография
def pad(byte_array):
    BLOCK_SIZE = 16
    pad_len = BLOCK_SIZE - len(byte_array) % BLOCK_SIZE
    return byte_array + (bytes([pad_len]) * pad_len)
def unpad(byte_array):
    last_byte = byte_array[-1]
    return byte_array[0:-last_byte]
def encrypt(key, iv, message):
    byte_array = str(message).encode("UTF-8")
    padded = pad(byte_array)
    obj = AES.new(key.encode("UTF-8"), AES.MODE_CBC,iv.encode('UTF-8'))
    ciphertext = obj.encrypt(padded) #шифртекст как биты
    ct1 = base64.b64encode(ciphertext).decode("UTF-8") #шифртекст как строка
    return(ct1)
def decrypt(key, iv, message):
    byte_array = base64.b64decode(message)
    cipher = AES.new(key.encode("UTF-8"), AES.MODE_CBC,iv.encode('UTF-8'))
    decrypted_padded = cipher.decrypt(byte_array)
    decrypted = unpad(decrypted_padded)
    text = decrypted.decode("UTF-8")
    return(text)

#шаг 3 (Трент посылает Алисе ID Боба, ключ сессии, её рандомную последовательность и рандомное число Боба, которые зашифрованы ключом Алисы,
#       а также ID Алисы и ключ сессии, зашифрованный ключом Боба)

async def trent(websocket, path, stop_request):
    keys = open("keys.txt", 'r')
    line=keys.readlines()
    Alice_key=line[0]
    Alice_iv=line[1]
    Bob_key=line[2]
    Bob_iv=line[3]
    keys.close
    # он так или иначе знает, как связаться с пользователями, так что пусть будет так
    bobport = '8888'
    arisuport = '9999'
    bob_message = json.loads(await websocket.recv())
    decrypto_data = decrypt(Bob_key, Bob_iv, bob_message['data']) # расшифровать
    print(decrypto_data)
    decrypt_data = json.loads(decrypto_data.replace('\'', '\"'))
    noncea = decrypt_data['noncea']
    nonceb = decrypt_data['nonceb']
    session_key = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase  #магия для создания строки длиной 16 чтобы методы шифрования
    + string.digits) for _ in range(16))                                                 #и расшифрования работали без дополнительных манипуляций
    # шифруем полностью ключом Алисы
    for_arisu = {
        "reciever": 'bobu',
        "session_key": session_key,
        "noncea": noncea,
        "nonceb": nonceb
    }
    # шифруем полностью ключом Боба
    for_bobu = {
        "sender": 'arisu',
        "session_key": session_key
    }
    for_arisu_enc = encrypt(Alice_key, Alice_iv, for_arisu)
    for_bobu_enc = encrypt(Bob_key, Bob_iv, for_bobu)
    finalmessage = json.dumps([for_arisu_enc, for_bobu_enc])
    print("Step 3 message: " + finalmessage)
    uri = "ws://localhost:" + arisuport
    while True:
        try:
            arisu = await websockets.connect(uri) 
            break
        except:
            time.sleep(2)
    await arisu.send(finalmessage)
    stop_request.set()
    
async def main():
    exit_switch = 0
    trentport = input("Input port to listen on: ")
    stop_request = asyncio.Event()
    arisu_handler = functools.partial(trent, stop_request = stop_request)
    server = await websockets.serve(arisu_handler, "localhost", trentport)
    await stop_request.wait()
    #stop_request.clear()
    server.close()

asyncio.run(main())
