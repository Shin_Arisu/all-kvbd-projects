import asyncio
import functools
import websockets
import rsa

async def bobu(websocket, path, stop_request):
    (B_pubkey, B_privkey) = rsa.newkeys(1024, poolsize=8) #генерация ключей Боба
    print("Getting public key...")
    arisu_pubkey = rsa.PublicKey.load_pkcs1(await websocket.recv(), format='DER') #открытый ключ алисы получен
    print("Sending public key...") 
    await websocket.send(B_pubkey.save_pkcs1(format='DER')) #отправляем открытый ключ Боба в подходящем формате
    arisumessage = await websocket.recv() #ждём сообщение Алисы
    print("Recieving message: " + str(arisumessage) + "...")
    input_answer = input("Input answer: ")
    answer = rsa.encrypt(input_answer.encode('utf8'), arisu_pubkey)
    firsthalf = answer[:len(answer)//2]
    secondhalf = answer[len(answer)//2:]
    await websocket.send(firsthalf)
    print("Sent first half")
    arisumessage = arisumessage + await websocket.recv()
    arisumessage= rsa.decrypt(arisumessage, B_privkey) #Боб расшифровывает сообщение Алисы своим закрытым ключом
    print("Recieved message: " + str(arisumessage)[2:-1])
    await websocket.send(secondhalf) #Боб отправляет вторую половину своего зашифрованного сообщения
    print("Sent second half")
    stop_request.set()

async def arisu():
    (A_pubkey, A_privkey) = rsa.newkeys(1024, poolsize=8) #генерация ключей Алисы
    bobport = input("Enter reciever's port: ")
    uri = "ws://localhost:" + bobport
    bob = await websockets.connect(uri)
    print("Sending public key...")
    await bob.send(A_pubkey.save_pkcs1(format='DER')) #открытый ключ Алисы отправлен
    input_message = input('Input message: ')
    print("Getting public key...")
    bob_pub_key  = rsa.PublicKey.load_pkcs1(await bob.recv(), format='DER') #получили открытый ключ Боба
    message = rsa.encrypt(input_message.encode('utf8'), bob_pub_key) #Алиса шифрует сообщение открытым ключом Боба
    firsthalf = message[:len(message)//2]
    secondhalf = message[len(message)//2:]
    await bob.send(firsthalf) #Алиса отправляет первую половину
    print("Sent first half")
    bobmessage = await bob.recv() #Получаем сообщение Боба (первая половина)
    print ("Recieving message: " + str(bobmessage) + "...")
    await bob.send(secondhalf) #Алиса отправляет вторую половину
    print("Sent second half")
    bobmessage = bobmessage + await bob.recv() #Получаем сообщение Боба (вторая половина)
    bobmessage = rsa.decrypt(bobmessage, A_privkey)
    print("Recieved message: " + str(bobmessage)[2:-1])
    
async def main():
    exit_switch = 0
    while exit_switch == 0:
        switch = input('Enter mode (rec, send or exit): ')
        if switch == 'rec':
            arisuport = input('Enter port to listen on: ')
            stop_request = asyncio.Event()
            arisu_handler = functools.partial(bobu, stop_request = stop_request)
            server = await websockets.serve(arisu_handler, "localhost", arisuport)
            await stop_request.wait()
            stop_request.clear()
            server.close()
        elif switch == 'send':
            await arisu()
        elif switch == 'exit':
            exit_switch = 1

asyncio.run(main())
