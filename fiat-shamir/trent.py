import asyncio
import functools
import websockets
import json
import time
import os
import base64
import random, string
import secrets
from Crypto.Util import number

async def initgen():
    # тут нужны большие простые числа, как для rsa
    #p = random.randrange(1488)
    #q = random.randrange(1488)
    p = number.getPrime(1024)
    q = number.getPrime(1024)
    #print("p", p)
    #print("q", q)
    n = p * q
    return n

async def trent(websocket, path, stop_request, n):
    # он так или иначе знает, как связаться с пользователями, так что пусть будет так
    bobport = '8888'
    arisuport = '9999'
    print("N = " + str(n))
    global arisu_key
    message = json.loads(await websocket.recv())
    # вначале отправляем n, получаем ключ, потом отправляем ключ и n бобу
    if message['sender'] == 'arisu':
        await websocket.send(str(n))
        vi = json.loads(await websocket.recv())
        arisu_key = vi['vi']
    else:
        for_bob = {
            "reciever": 'bob',
            "key": arisu_key,
            "n": n
        }
        await websocket.send(json.dumps(for_bob))
        stop_request.set()


arisu_key=0

async def main():
    # генерируем n
    n = await initgen()
    #print("n = ", n)
    exit_switch = 0
    trentport = input("Input port to listen on: ")
    stop_request = asyncio.Event()
    arisu_handler = functools.partial(trent, stop_request = stop_request, n = n)
    server = await websockets.serve(arisu_handler, "localhost", trentport)
    await stop_request.wait()
    #stop_request.clear()
    server.close()

asyncio.run(main())
