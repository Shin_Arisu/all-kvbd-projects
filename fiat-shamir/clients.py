import asyncio
import functools
import websockets
import random
from Crypto.Cipher import AES
import json
import os
import base64
import time
from Crypto.Util import number

async def bobu(websocket, path, stop_request, trentport):
    round_num = int(input("Input number of rounds: "))
    await websocket.recv()
    uri = "ws://localhost:" + trentport
    trent = await websockets.connect(uri)
    message = {
        "sender": 'bob'
    }
    message = json.dumps(message)
    await trent.send(message)
    trent_ans = json.loads(await trent.recv())
    arisu_key = int(trent_ans['key'])
    n = int(trent_ans['n'])
    round_succ = 0
    fail_flag = 0
    #print("this is an ARISU KEY", arisu_key)
    await websocket.send(str(round_num))
    print ("Round num:" + str(round_num))
    while round_succ < round_num and fail_flag != 1:
        x = int(await websocket.recv())
        e = random.randrange(2)
        print ("e = " + str(e))
        await websocket.send(str(e))
        y = int(await websocket.recv())
        print ("y = " + str(y))
        if y != 0:
            print ("1: " + str((y**2)%n) + " " + "2: " + str((x*(arisu_key**e))%n))
            #print ("x", x, "arisu_key", arisu_key)
            #       заглушка, тут:     if (y**2)%n == (x*arisu_key**e)%n: , вроде
            if ((y**2)%n == (x*(arisu_key**e))%n):
                round_succ += 1
                await websocket.send(str(0))
                print ("Round passed")
                print ("succ = " + str(round_succ))
            else:
                fail_flag = 1
                await websocket.send(str(1))
                print ("Round failed")
        else:
            await websocket.send(str(1))
            print ("Round 0")
    if round_succ == round_num:
        await websocket.send("success")
    else:
        await websocket.send("failed")
    stop_request.set()

async def arisu(arisuport, trentport):
    bobport = '8888'
    uri = "ws://localhost:" + trentport
    trent = await websockets.connect(uri)
    message = {
        "sender": 'arisu',
    }
    message = json.dumps(message)
    await trent.send(message)
    n = int(await trent.recv())
    # s нужно взаимнопростое с n
    #s = random.randrange(n-1)
    s = number.getPrime(1023)
    v = (s**2)%n
    message = {
        "sender": 'arisu',
        "vi": v
    }
    message = json.dumps(message)
    await trent.send(message)
    uri = "ws://localhost:"+bobport
    bob = await websockets.connect(uri)
    #просто тыкаем боба, чтобы начать работу
    await bob.send("111")
    #получаем количество раундов
    round_num = int(await bob.recv())
    round_succ = 0
    fail_flag = 0
    print ("Round numbers:" + str(round_num))
    #начинаем веселые проверки, тут математику я не проверял
    while round_num > round_succ and fail_flag != 1:
        r = random.randrange(n-1)
        x = (r**2)%n
        await bob.send(str(x))
        e = int(await bob.recv())
        print ("e = " + str(e))
        y = (r*(s**e))%n
        print ("y = " + str(y))
        await bob.send(str(y)) # тут была заглушка! 
        fail_flag = int(await bob.recv())
        print("flag = " + str(fail_flag))
        if y != 0 and fail_flag != 1:
            round_succ += 1
            print ("Round passed")
            print ("succ" + str(round_succ))
    print ("out")
    fin = await bob.recv()
    print ("Result: " + fin)

async def main():
    exit_switch = 0
    trentport = input("Input Trent's port: ")
    while exit_switch == 0:
        switch = input('Enter mode (rec, send or exit): ')
        if switch == 'rec':
            bobuport = 8888
            stop_request = asyncio.Event()
            bobu_handler = functools.partial(bobu, stop_request = stop_request, trentport = trentport)
            server = await websockets.serve(bobu_handler, "localhost", bobuport)
            await stop_request.wait()
            stop_request.clear()
            server.close()
        elif switch == 'send':
            arisuport = 9999
            await arisu(arisuport, trentport)
        elif switch == 'exit':
            exit_switch = 1

asyncio.run(main())
